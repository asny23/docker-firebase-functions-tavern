# docker-firebase-functions-tavern

[Container Registry](https://gitlab.com/asny23/docker-firebase-functions-tavern/container_registry)

(Older images available on [Docker Hub](https://hub.docker.com/r/asny23/firebase-functions-tavern))

`cimg/base` based Docker image for firebase functions CI

Current latest=`cimg2022.12-firebase11.16.1`

- FROM `cimg/base:2022.12-20.04`
- [Volta](https://docs.volta.sh/guide/)
- [firebase-tools](https://www.npmjs.com/package/firebase-tools)@`11.16.1`
- tavern `1.24.1`
